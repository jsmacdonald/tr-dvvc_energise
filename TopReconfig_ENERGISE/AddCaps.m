function [Q_Load_pu] = AddCaps(Q_Load_pu, S_Base, Model)
% Adds capacitors to a network with fixed reactive power input if they
% exist and their is an input file in the directory.
try
    fn = ['Caps_' Model '.csv'];
    T = readtable(fn);

    NodeIDs = T.NodeID;
    Q_Caps = T.kVAR;

    Q_Load_pu(NodeIDs,:)=Q_Load_pu(NodeIDs,:)+Q_Caps/S_Base;
end