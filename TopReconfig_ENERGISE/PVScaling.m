PV_RatedCAP = [1462; 1775; 944; 628; 812; 956; 648];

load('RPU/OG Data/PV.mat')

PV_Cap_in = max(sqrt(PV.P_Sunny.^2+PV.Q_Sunny.^2));

SunP = zeros(length(PV_RatedCAP),24);
SunQ = zeros(length(PV_RatedCAP),24);
RainP = zeros(length(PV_RatedCAP),24);
RainQ = zeros(length(PV_RatedCAP),24);

n=1;
noise = 0;
noise_threshhold = 0;
timesteps = 24;

for PV_Cap_out = PV_RatedCAP'
   [ SunP(n,:) , SunQ(n,:) ] = PV_scaling(PV.P_Sunny, PV.Q_Sunny, PV_Cap_in, PV_Cap_out, 0, 24, 0);
   [ RainP(n,:) , RainQ(n,:) ] = PV_scaling(PV.P_Rainy, PV.Q_Rainy, PV_Cap_in, PV_Cap_out, 0, 24, 0);
   n=n+1;
end


function [ PV_P_t, PV_Q_t ]=PV_scaling(PV_P, PV_Q, PV_Cap_in, PV_Cap_out, noise, timesteps, noise_threshhold)
    Timesteps_in = length(PV_P);
    PV_P_t=zeros(1,timesteps);
    PV_Q_t=zeros(1,timesteps);
    DelT = floor(Timesteps_in/timesteps);
    n = 0;
    Cap_multiplier = PV_Cap_out/PV_Cap_in;
    for t=1:DelT:(Timesteps_in-DelT+1)
        n=n+1;
        PV_P_t(n)=Cap_multiplier*mean(PV_P(t:(t+DelT-1)));
        PV_Q_t(n)=Cap_multiplier*mean(PV_Q(t:(t+DelT-1)));
    end
    MaxPVP=max(PV_P_t);
    Min_noisy = noise_threshhold*MaxPVP;

    %PV_P_t = PV_P/PV_Cap_in*PV_Cap_out; %Appropriately size the real power time series
    %PV_Q_t = PV_Q/PV_Cap_in*PV_Cap_out; %Appropriately size the reactive power time series
    
    QtoP = sum(PV_Q_t)/sum(PV_P_t); %Ratio of total Q to total P
    
    PV_P_t(PV_P_t>=Min_noisy)=PV_P_t(PV_P_t>=Min_noisy)+noise*PV_Cap_out*randn(size(PV_P_t(PV_P_t>=Min_noisy))); %add a random normal noise to the real power output when real power is non-zero
    PV_P_t(PV_P_t<0)=0; %If noise dropped real power to below zero, make it zero.
    
    PV_Q_t(PV_Q_t~=0)=PV_Q_t(PV_Q_t~=0)+noise*PV_Cap_out*QtoP*randn(size(PV_Q_t(PV_Q_t~=0))); %add a random normal noise to reactive power 
end
