function [A, b, Aeq, beq] = powerflow_linear(R, X, C, timesteps, Num_Buses, Load_P, Load_Q, PV_Loc)


% mapping the cvx variables to locations in the fmincon optimization variables: vars
%Line_P = vars(1:Num_Buses,:);
%Line_Q = vars((1*Num_Buses+1):2*Num_Buses,:);
%Line_V2 = vars((2*Num_Buses+1):3*Num_Buses,:);
%Pstar_PV = vars((3*Num_Buses+1):4*Num_Buses,:);
%Qstar_PV = vars((4*Num_Buses+1):5*Num_Buses,:);

A = []; %no inequality constraints in powerflow equations
b = []; %no inequality constraints in powerflow equations

%Equality constraints: Aeq*x-beq = 0
Aeq = zeros(5*Num_Buses*timesteps, 5*Num_Buses*timesteps);
beq = zeros(5*Num_Buses*timesteps,1);

id_mat = diag(ones(Num_Buses,1),0); %I will need this identity matrix 
id_mat_0 = id_mat;
id_mat_0(1,1) = 0;% This one has a 0 on the top line

for t = 1:timesteps
    %first we make a Aeq that is only for this time point.
    Aeq_sub = zeros(5*Num_Buses,5*Num_Buses);
    
    Ct = C(:,:,t);
    C_trans = [zeros(1,Num_Buses) ; Ct(:,2:end)'];
    Rt = R(:,:,t);
    Xt = X(:,:,t);
    
    %The real power flow portion:
    %  cvx: Line_P(:,t) == Load_P(:,t) - PV_Loc * Pstar_PV(:,t) + Ct * Line_P(:,t);
    Aeq_sub(1:Num_Buses,1:Num_Buses) = Ct-id_mat;% multiplied by Line_P
    Aeq_sub(1:Num_Buses,(3*Num_Buses+1):4*Num_Buses) = -PV_Loc;% multiplied by PV_Pstar
    
    %The reactive power portion:
    %  cvx: Line_Q(:,t) == Load_Q(:,t) - PV_Loc * Qstar_PV(:,t) + Ct * Line_Q(:,t);
   
    Aeq_sub((1*Num_Buses+1):2*Num_Buses,(1*Num_Buses+1):2*Num_Buses) = Ct-id_mat;% multiplied by Line_Q
    Aeq_sub((1*Num_Buses+1):2*Num_Buses,(4*Num_Buses+1):5*Num_Buses) = -PV_Loc;% multiplied by PV_Qstar
    
    %The voltage portion:
    %  cvx: C_trans * Line_V2(:,t) == Line_V2(2:end,t) + 2 * Rt(2:end,2:end) * Line_P(2:end,t)+ 2 * Xt(2:end,2:end) * Line_Q(2:end,t);
    Aeq_sub((2*Num_Buses+1):3*Num_Buses,1:Num_Buses) = 2*Rt;
    Aeq_sub((2*Num_Buses+1):3*Num_Buses,(1*Num_Buses+1):2*Num_Buses) = 2*Xt;
    Aeq_sub((2*Num_Buses+1):3*Num_Buses,(2*Num_Buses+1):3*Num_Buses) = id_mat_0-C_trans; 
    
    %Now we need to add something to Aeq_sub for the nodes for the
    %locations that don't have voltage defined by a To node:
    Nodes = 1:Num_Buses;
    No_Tos = Nodes(sum(Ct,1)==0);
    for nd = No_Tos
        Aeq_sub((2*Num_Buses+nd),2*Num_Buses+nd) = 1;
    end
    
    %Now we need to add what we just created to the Aeq that represents all
    %timesteps included:
    
    base_point = (t-1)*5*Num_Buses; %The first point that we in either the matrix Aeq or the vector Beq for this timestep iteration
    
    Aeq(base_point+1:base_point+5*Num_Buses,base_point+1:base_point+5*Num_Buses) = Aeq_sub;
    
    % beq is just the real and reactive load, fill in the
    %correct locations in beq
    beq((1+base_point):base_point+Num_Buses,1) = -Load_P(:,t);
    beq((1+base_point+Num_Buses):base_point+2*Num_Buses,1) = -Load_Q(:,t);
    
    %we need to define all nodes, including the substation node that does
    %not have voltge defined by another node on the network:
    for nd = No_Tos
        beq((nd+base_point+2*Num_Buses),1) = 1; %Force the Root Node to have a Voltage = 1 pu
    end
end