% Author: Jason MacDonald
% Date: May 11th, 2018


function [Pstar_PV, Qstar_PV, SwitchStates, Bad_Input_File, Feasible_Flag] = DA_Network_Op_Main_qp_scen(timesteps, V_base, S_base, ModelName, perc_red_threshhold, loop_red_threshhold, linearization_180, Weather, PVScale, LdScale)
% This is the function to be turned into a JAR file for the DA Network
% Optimization that minimizes losses via topology reconfiguration

%% Inputs common across scenarios (these are not needed as this is set up as a function)

% %Inputs to be removed once this is a function:
% timesteps = 24; %This is an hourly DA optimization with a 24-hour horizon
% V_base = 12667;%V
% S_base = 5000;%kVA
% ModelName = 'IEEE33';
% perc_red_threshhold =  0.005; %the percentage (0.01 = 1%) at which we continue to make switching actions.
% loop_red_threshhold =  0.01; %the percentage (0.01 = 1%) at which we continue to run OPF/switch loops.

%% Read and prepare inputs
%Calculate the Base Impedance for per unit calculations
Start_time = datetime;
Z_base = V_base^2/(S_base*10^3);%Ohms  %This should be replaced with something that performs this calculation taking into account the transformer steps in the future.

% Load up the network
[ Network , NetworkHeaders, Num_Nodes, Bad_Flag1 ] =  GetPSNetwork(ModelName);
[ NetworkModSw, NetworkModHeaders, Bad_Flag2 ] = GetSwitches(Network, NetworkHeaders, ModelName);

line = NetworkModSw;
line(:,3:4) = line(:,3:4)/Z_base; %put the impedance numbers in per_unit.

%Load up the PV Parameters
[ PV_Cap, PV_Loc, PV_Nodes, Bad_Flag3 ] = GetPVGens(ModelName, Num_Nodes);
PV_Cappu = PV_Cap/S_base*PVScale; %Per Unitize the PV_Cap

%Get Load time series
if strcmp(ModelName,'RPU')
    DeltaT = 1/timesteps; %timestep in days (since that is the unit of datetime objects in matlab)
    [Load_P, Load_Q, Final_P_PV, Final_Q_PV] = RPU_CYME_RESULT_LOADER(DeltaT);
    Bad_Flag4 = false;
else
    [ Load_P, Load_Q, Bad_Flag4 ] = GetLoadForecast(ModelName, Num_Nodes, timesteps);
end
%Put it in Per Unit
Load_Ppu = Load_P/S_base*LdScale;
Load_Qpu = Load_Q/S_base*LdScale;

[Load_Qpu] = AddCaps(Load_Qpu, S_base, ModelName);

[PV_P_t, PV_Q_t, Bad_Flag5] = GetPVForecast(ModelName, Num_Nodes, PV_Nodes, timesteps, Weather);
%Put it in Per Unit                
PV_P_tpu = PV_P_t/S_base*PVScale;
PV_Q_tpu = PV_Q_t/S_base*PVScale;
                
% Any Bad Inputs?
Bad_Input_File = Bad_Flag1 || Bad_Flag2 || Bad_Flag3 || Bad_Flag4 || Bad_Flag5;

%% Initialize 

% Initialize the switches for all time in the model 
SwitchStates = zeros(length(line(:,5)),timesteps);
for t = 1:timesteps
    SwitchStates(:,t) = line(:,5);
end

%Create the initial conditions for the R, X, and Connectivity
%matrices (will not work on later switchstates because of
%reverse direction in loops)
[R, X, C] = Model_Form(line, SwitchStates, Num_Nodes, timesteps);
              
disp('Run OPF for Initialization')
Line_Cap = []; %There are no line capacities in IEEE33 feeder data.
tic
[Line_P, Line_Q, Line_V2, Pstar_PV, Qstar_PV, cvx_optval, cvx_status] = Loss_Min_OPF_qp(Load_Ppu, Load_Qpu, timesteps, PV_P_tpu, PV_Q_tpu, PV_Cappu, PV_Loc, Line_Cap, Num_Nodes, R, X, C, linearization_180 );
toc

Init_OPF = cvx_optval;
Current_OPF = cvx_optval;
                                
It_results = {{Line_P, Line_Q, Line_V2, Pstar_PV, Qstar_PV, cvx_optval, cvx_status, SwitchStates, C, R, X}};


if strcmp(cvx_status,'Solved')
    Iterations = 0; %Establish the number of iterations are required to determine results
    Loop_logical =  true;
    Feasible_Flag = true;

    while Loop_logical && Iterations <= 4
        FullDayResults = zeros(4,24);

        disp('Begin Switching')
        for t = 1:24
            [SwitchStates, C, R, X, Line_P, Line_Q, Tot_switches, Final_Losses, OG_Losses, Tot_prct_red] = perform_switching(t, SwitchStates, C, R, X, Line_P, Line_Q, line, perc_red_threshhold, S_base);
            FullDayResults(:,t) = [Tot_switches ; OG_Losses ; Final_Losses ; Tot_prct_red ];
        end
        Switching_Losses = sum(FullDayResults(3,:));

        In_Loop_Reduction = (Current_OPF - Switching_Losses)/Current_OPF;

        if In_Loop_Reduction >= loop_red_threshhold
            Iterations = Iterations+1;
            disp('Run OPF on Switched Model')
            [Line_P_new, Line_Q_new, Line_V2_new, Pstar_PV_new, Qstar_PV_new, cvx_optval, cvx_status] = Loss_Min_OPF_qp(Load_Ppu, Load_Qpu, timesteps, PV_P_tpu, PV_Q_tpu, PV_Cappu, PV_Loc, Line_Cap, Num_Nodes, R, X, C, linearization_180 );
            if strcmp(cvx_status,'Solved')
                Line_P = Line_P_new;
                Line_Q = Line_Q_new;
                Line_V2 = Line_V2_new;
                Pstar_PV = Pstar_PV_new;
                Qstar_PV = Qstar_PV_new;
                Current_OPF = cvx_optval;

                Out_loop_reduction = abs((Switching_Losses - Current_OPF)/Switching_Losses);
                Loop_logical = (Out_loop_reduction >= loop_red_threshhold);

                It_results{end+1} = {Line_P, Line_Q, Line_V2, Pstar_PV, Qstar_PV, cvx_optval, cvx_status, SwitchStates,  C, R, X, FullDayResults};
            else
                Loop_logical = false;
                It_results{end+1} = {};
            end
        else
            Loop_logical = false;
        end

    end
%     Cur_Time = datestr(datetime);
%     % Write the Line Conditions:
%     fn = ['Results_' 'Line_P' '_' Cur_Time '.csv'];
%     csvwrite(fn,full(Line_P));
%     
%     fn = ['Results_' 'Line_Q' '_' Cur_Time '.csv'];
%     csvwrite(fn,full(Line_Q));
%     
%     fn = ['Results_' 'Line_V2' '_' Cur_Time '.csv'];
%     csvwrite(fn,full(Line_V2));
%     
%     % Write the Optimal PV setpoints and switches
%     fn = ['Results_' 'Pstar_PV' '_' Cur_Time '.csv'];
%     csvwrite(fn,full(Pstar_PV));
%     
%     fn = ['Results_' 'Qstar_PV' '_' Cur_Time '.csv'];
%     csvwrite(fn,full(Qstar_PV));
%     
%     fn = ['Results_' 'SwitchStates' '_' Cur_Time '.csv'];
%     csvwrite(fn,SwitchStates);
    
    %Create some meta results and save them
    Num_Switches = zeros(1,timesteps);
    Switched = cell(1,timesteps);
    for t = 1:timesteps
        Num_Switches(1,t) = sum(line(:,5)~=SwitchStates(:,t));
        Switched{1,t} = line(line(:,5)~=SwitchStates(:,t),[1, 2, 5]);
    end
    
    End_time = datetime;
    Runtime = End_time - Start_time;
    save(['Results/Results_' Weather 'PV' num2str(round(100*PVScale)) '_Ld' num2str(round(100*LdScale)) '.mat'], 'It_results','Num_Switches', 'FullDayResults','Switched','Runtime')
    
%     fn = ['Results_' 'Num_Switches' '_' Cur_Time '.csv'];
%     csvwrite(fn,Num_Switches);
    
else
    Feasible_Flag = false;
end

end
                
%% Functions
%%%%%%%%%%%% Functions for reading the data %%%%%%%%%%%%%%%%%%
function [PV_P_t, PV_Q_t, Bad_Flag] = GetPVForecast(ModelName, Num_Nodes, PV_Nodes, timesteps, Weather)
    filenameP = ['PV_Pmax_' ModelName '_' Weather '.csv'];
    filenameQ = ['PV_Qunctrl_' ModelName '_' Weather '.csv'];
    Bad_Flag = false; 
    TP = 0;
    TQ = 0;
    PV_P_t = zeros(Num_Nodes,timesteps);
    PV_Q_t = zeros(Num_Nodes,timesteps);
    try
       TP = readtable (filenameP);
       TQ = readtable (filenameQ);
    catch
        Bad_Flag = true;
    end
    HeadersP = TP.Properties.VariableNames;
    HeadersQ = TQ.Properties.VariableNames;
    %make sure TP and TQ are the same size
    if ~ismember(size(TP),size(TQ),'rows')
        Bad_Flag = true;
    end
    
    PV_NodesP = eval(['TP.' HeadersP{1}]);
    PV_NodesQ = eval(['TQ.' HeadersQ{1}]);
    
    %make sure the nodes with solar are the same
    if ~(isequal(PV_NodesP,PV_NodesQ)) || ~(isequal(PV_Nodes,PV_NodesP))
        Bad_Flag = true;
    end
    
    %make sure that the number of timesteps is correct
    if ~(size(TP,2)==timesteps+1) || ~(size(TQ,2)==timesteps+1) || Bad_Flag
        Bad_Flag = true;
    else
        indices=0;
        for n = 1: Num_Nodes
            if ismember(n,PV_Nodes)
                indices = indices+1;
                for t = 1:timesteps
                    PV_P_t(n,t) = eval(['TP.' HeadersP{t+1} '(indices,1)']);
                    PV_Q_t(n,t) = eval(['TQ.' HeadersQ{t+1} '(indices,1)']);
                end
            end
        end
    end 
end

function [ Load_P, Load_Q, Bad_Flag ] = GetLoadForecast(ModelName, Num_Nodes, timesteps)
    filenameP = ['Load_Forecast_P_' ModelName '.csv'];
    filenameQ = ['Load_Forecast_Q_' ModelName '.csv'];
    Bad_Flag = false; 
    TP = 0;
    TQ = 0;
    try
       TP = csvread (filenameP);
       TQ = csvread (filenameQ);
    catch
        Bad_Flag = true;
    end
    
    if ~(size(TP,1)==Num_Nodes) || ~(size(TQ,1)==Num_Nodes)
        Bad_Flag = true;
    elseif ~(size(TP,2)==timesteps) || ~(size(TP,2)==timesteps)
        Bad_Flag = true;
    end
    if ~ismember(size(TP),size(TQ),'rows')
        Bad_Flag = true;
    end
    Load_P = TP;
    Load_Q = TQ;
    
end

function [ PV_Cap, PV_Loc, PV_Nodes, Bad_Flag ] = GetPVGens(ModelName, Num_Nodes)
    filename = ['PVgenerators_' ModelName '.csv'];
    Bad_Flag = false; 
    T = 0;
    try
        T = readtable(filename);
    catch
        Bad_Flag = true;
    end
    Headers = T.Properties.VariableNames;
    PV_Cap = zeros (Num_Nodes, 1);
    PV_Loc = zeros (Num_Nodes, Num_Nodes);
    PV_Nodes = eval(['T.' Headers{1}]);
    PV_Caps = eval(['T.' Headers{2}]);
    for n = 1: Num_Nodes
        if ismember(n,PV_Nodes)
            PV_Cap(n,1) = PV_Caps(ismember(PV_Nodes,n),1);
            PV_Loc(n,n) = 1;
        end
    end
end

function [ NetworkModel, NetworkHeaders, Num_Nodes, Bad_Flag ] = GetPSNetwork(ModelName)
    filename = ['Lines_' ModelName '.csv'];
    Bad_Flag = false; 
    T = 0;
    try 
        T = readtable(filename);
    catch
        Bad_Flag = true;
    end
    NetworkHeaders = T.Properties.VariableNames;
    NetworkModel = zeros(size(T)); %In this case, the networkmodel refers to the description of the lines that make up the network.
    for l = 1:width(T)
        NetworkModel(:,l) = eval(['T.' NetworkHeaders{l}]);
    end
    % Identify the number of nodes by taking the max value of the node list
    % in both the sending and receiving columns of the table.  The sending
    % column must be the first. The receiving column must be the second.
    % The nodes must be identified in ascending order from 1 (the root node) to
    % N (the number of nodes). Alternate node identification will make the
    % whole thing fail.
    Num_Nodes = max([ eval(['T.' NetworkHeaders{1}]) ; eval(['T.' NetworkHeaders{2}]) ]); 
    
end

function [ NetworkModSw, NetworkModHeaders, Bad_Flag ] = GetSwitches(NetworkModel, NetworkHeaders, ModelName)
    filename =  ['Switches_' ModelName '.csv'];
    Bad_Flag = false;
    T = 0;
    try 
        T = readtable(filename);
    catch
        Bad_Flag = true;
    end
    Headers = T.Properties.VariableNames;
    NetworkModHeaders = NetworkHeaders;
    NetworkModHeaders{end+1} = Headers{3};
    NetworkModHeaders{end+1} = 'SwitchExists';
    NetworkModSw = [NetworkModel ones(size(NetworkModel,1),1) zeros(size(NetworkModel,1),1)];
    for row = 1:height(T)
        NodePair = [eval(['T.' Headers{1} '(row)']), eval(['T.' Headers{2} '(row)'])];
        InitCond = eval(['T.' Headers{3} '(row)']);
        if ~ismember(NodePair, NetworkModel(:,1:2),'rows')
            NodePair = [NodePair(2) NodePair(1)];
        end
        NetworkModSw(ismember(NetworkModSw(:,1:2),NodePair,'rows'),end-1:end) = [InitCond 1];
    end
end

%%%%%%%%%%%% Functions for preparing model input %%%%%%%%%%%%%%%%%%

function [R, X, C] = Model_Form(line, SwitchStates, Num_Buses, timesteps)
    %line_headings is {'from_bus' 'to_bus' 'r' 'x' 'Initial Switch State'}
    R = zeros(Num_Buses, Num_Buses,timesteps);
    X = R;
    C = R;
    for t = 1:timesteps
        OnLines=line(SwitchStates(:,t)==1,:);
      %Initialize what we'll call the Connectivity Matrix, which indicates a directed graph of the connections between nodes.
        for l = 1:size(OnLines,1)
            R(OnLines(l,2),OnLines(l,2),t)=OnLines(l,3);
            X(OnLines(l,2),OnLines(l,2),t)=OnLines(l,4);
            C(OnLines(l,1),OnLines(l,2),t)=1;
        end
    end
end

%%%%%%%%%%%% Function to run the OPF %%%%%%%%%%%%%%%%%%%%%%%%%

% function [Line_P, Line_Q, Line_V2, Pstar_PV, Qstar_PV, cvx_optval, cvx_status] = Loss_Min_OPF(Load_P, Load_Q, timesteps, PV_P_t, PV_Q_t, PV_Cap, PV_Loc, Num_Buses, R, X, C )
%     cvx_begin quiet
%         expression F
%         
%         variable Line_P(Num_Buses,timesteps) 
%         variable Line_Q(Num_Buses,timesteps)
%         variable Line_V2(Num_Buses,timesteps) 
%         variable Pstar_PV(Num_Buses,timesteps) nonnegative 
%         variable Qstar_PV(Num_Buses,timesteps)
% 
%         
%         %Defining the Objective
%         F=0;
%         for t=1:timesteps
%             F =  F + quad_form( Line_P(:,t), R(:,:,t) ) + quad_form( Line_Q(:,t), R(:,:,t) );
%         end
%         
%         minimize F
%         
%         subject to
%             %Line_V2(1,:) == 1;
%             for t = 1:timesteps
%                 Ct = C(:,:,t);
%                 C_trans = Ct(:,2:end)';
%                 Rt = R(:,:,t);
%                 Xt = X(:,:,t);
%                 Line_P(:,t) == Load_P(:,t) - PV_Loc * Pstar_PV(:,t) + Ct * Line_P(:,t);
%                 Line_Q(:,t) == Load_Q(:,t) - PV_Loc * Qstar_PV(:,t) + Ct * Line_Q(:,t);
%                 C_trans * Line_V2(:,t) == Line_V2(2:end,t) + 2 * Rt(2:end,2:end) * Line_P(2:end,t)+ 2 * Xt(2:end,2:end) * Line_Q(2:end,t);
%                 0.95^2 <= Line_V2(:,t) <= 1.05^2;
%                 for n = 1:Num_Buses
%                     Pstar_PV(n,t) <= PV_P_t(n,t);
%                     Pstar_PV(n,t)^2 + Qstar_PV(n,t)^2 <= PV_Cap(n,1)^2;
%                 end
%             end
%     cvx_end
%     disp(cvx_status)
%     disp(cvx_optval)
% end

%%%%%%%%%%%%% Functions for the Loss minimizing switching algorithm %%%%%%%

function [SwitchStates, C, R, X, Line_P, Line_Q, Tot_switches, Final_Losses, OG_Losses, Tot_prct_red] = perform_switching(t, SwitchStates, C, R, X, Line_P, Line_Q, line, perc_red_threshhold, S_base)
    OG_Losses = Line_P(:,t)'*R(:,:,t)*Line_P(:,t) + Line_Q(:,t)'*R(:,:,t)*Line_Q(:,t);
    SwitchStates_new = SwitchStates;
    ConMat = C(:,:,t);
    Make_Switch = 1; %for while loop purposes, we want to start by assuming that we'll need to make a switch
    Tot_switches = 0; %Going to tick this up for each topology change
    while Make_Switch==1
        %%% Under this should likely be the while loop?
        Tie_Lines=line(~logical(SwitchStates_new(:,t)),:);
        Starting_Losses = Line_P(:,t)'*R(:,:,t)*Line_P(:,t) + Line_Q(:,t)'*R(:,:,t)*Line_Q(:,t);
        Best_switches = zeros(size(Tie_Lines,1),5); % First, |Tie_line_snode|Tie_line_rnode|Open_sw_s|Open_sw_r|
        for l = 1:size(Tie_Lines,1)

            Tie_Line=Tie_Lines(l,:);

            To_Node = Tie_Lines(l,2);
            try
            [root_map_ToNode] = map_root(To_Node,ConMat);
                map_failed = false;
            catch
                map_failed = true;
                display(['Failed to Map from node: ' num2str(To_Node)])
            end

            %Map the way from the from-node to the root
            From_Node = Tie_Lines(l,1);
            if ~map_failed
                try
                    [root_map_FromNode] = map_root(From_Node,ConMat);
                    map_failed = false;
                catch
                    map_failed = true;
                    display(['Failed to Map from node: ' num2str(From_Node)])
                end
            end
            
            if ~map_failed 
                %The to-side of the loop created by closing the tie-line
                [To_side_logic, To_side_ix] = ismember(root_map_ToNode, root_map_FromNode);
                To_Side=root_map_ToNode(~To_side_logic);
                %disp(['To: ' num2str(To_Side)]);
                
                %The root of the loop created by closing the tie-line
                Common_map = root_map_ToNode(To_side_logic);
                Loop_root = Common_map(1);
                %disp(['Root: ' num2str(Loop_root)])
                
                %The from-side of the loop created by closing the tie-line
                [From_side_logic, From_side_ix] = ismember(root_map_FromNode, root_map_ToNode);
                From_Side=root_map_FromNode(~From_side_logic);
                %disp(['From: ' num2str(From_Side)]);
                
                %             if Tot_switches > 0
                %                 disp(Tot_switches)
                %             end
                
                %Id'ing net loads and lines
                [Pnet_Fr, Qnet_Fr, From_lines]=Branch_Loads(Line_P(:,t),Line_Q(:,t),From_Side, Loop_root, line);
                [Pnet_To, Qnet_To, To_lines]=Branch_Loads(Line_P(:,t),Line_Q(:,t),To_Side, Loop_root, line);
                
                %Determine Baseline Losses
                To_Losses = Branch_Losses (line, Pnet_To, Qnet_To, To_Side, Loop_root);
                From_Losses = Branch_Losses (line, Pnet_Fr, Qnet_Fr, From_Side, Loop_root);
                Baseline_Losses=To_Losses + From_Losses;
                
                [Switch_Pair, loss] = Switched_Losses (To_lines, From_lines, Tie_Line, Pnet_To, Pnet_Fr, Qnet_To, Qnet_Fr, To_Side, From_Side, Loop_root);
                
                %             if ismember(Tie_Line(1,1:2),[21,8],'rows')
                %                 disp(Tie_Line)
                %             end
                
                loss_Delta = Baseline_Losses - loss; %Determine the change in losses caused by this
            else
                loss_Delta = -inf;
                Switch_Pair = [0 0];
            end

            Best_switches(l,:) = [ From_Node To_Node Switch_Pair loss_Delta ];% Record loop results
        end

        [Loss_Delta, ix] = max(Best_switches(:,5));
        Percent_reduction = Loss_Delta/Starting_Losses;
        if Percent_reduction >= perc_red_threshhold
            Make_Switch = 1;
            Tie_in = [Best_switches(ix,1) Best_switches(ix,2)];
            Sw_out = [Best_switches(ix,3) Best_switches(ix,4)];
        else
            Tie_in = [];
            Sw_out = [];
            Make_Switch = 0;
            %disp(['Remaining Percent Reduction: ' num2str(Percent_reduction)])
            %disp('That''s all folks')
        end

        if Make_Switch == 1 %If we had enough of an improvement in losses to justify a switch.
            % We need to:
            % (1) Update SwitchStates
            SwitchStates_new(ismember(line(:,1:2),Tie_in,'rows'),t)=1;
            SwitchStates_new(ismember(line(:,1:2),Sw_out,'rows'),t)=0;
            % (2) Update C-Matrix

            % - In order to accomplish this, we'll need to use our branch maps from
            % before the switch
            [root_map_ToNode] = map_root(Tie_in(1),ConMat);
            [root_map_FromNode] = map_root(Tie_in(2),ConMat);
            [To_side_logic, To_side_ix] = ismember(root_map_ToNode, root_map_FromNode);
            To_Side=root_map_ToNode(~To_side_logic);
            [From_side_logic, From_side_ix] = ismember(root_map_FromNode, root_map_ToNode);
            From_Side=root_map_FromNode(~From_side_logic);
            Common_map = root_map_ToNode(To_side_logic);
            Loop_root = Common_map(1);

            % - Now let's get line and net load information
            Tie_Line = line(ismember(line(:,1:2),Tie_in,'rows'),:);
            [Pnet_Fr, Qnet_Fr, From_lines]=Branch_Loads(Line_P(:,t),Line_Q(:,t),From_Side, Loop_root, line);
            [Pnet_To, Qnet_To, To_lines]=Branch_Loads(Line_P(:,t),Line_Q(:,t),To_Side, Loop_root, line);

            Pnet = [Pnet_Fr ; Pnet_To];
            Qnet = [Qnet_Fr ; Qnet_To];

            %Figure out if the switch is on the "To" side or the "From" Side, then
            %extract the updated mappings
            if ismember(Sw_out,To_lines(:,1:2),'rows')
                [To_Side_new, From_Side_new] = post_switch_map(Sw_out, To_Side, From_Side);
                %[To_lines_new, From_lines_new, To_Side_new, From_Side_new] = New_branch_map(Sw_out, To_Side, From_Side, To_lines, From_lines, Tie_Line);
            elseif ismember(Sw_out,From_lines(:,1:2),'rows')
                [From_Side_new, To_Side_new] = post_switch_map(Sw_out, From_Side, To_Side);
                %[From_lines_new, To_lines_new, From_Side_new, To_Side_new] = New_branch_map(Sw_out, From_Side, To_Side, From_lines, To_lines, Tie_Line);
            else
                Bad_Map = Bad_Map+1;
                disp('The switch line must be recorded backward')
                Make_Switch = 0;
                To_Side_new =[];
                From_Side_new = [];
            end

            % - Now we should be able to use the mappings to create an updated
            % connectivity matrix
            %Initializing Changes to ConMat
            ConMat_new = ConMat;
            if Make_Switch == 1 %Don't make a change if we had a problem with the Switch definition
                ConMat_new(Tie_in(1),Tie_in(2))=1;
                if ConMat_new(Sw_out(1),Sw_out(2))==1
                    ConMat_new(Sw_out(1),Sw_out(2))=0;
                else
                    ConMat_new(Sw_out(2),Sw_out(1))=0;
                end
            end

            %Initialize Loads and R|X matrices so that we can change them as we
            %loop through our map to change connectivity matrix.
            Line_P_new = Line_P(:,t);
            Line_Q_new = Line_Q(:,t);
            R_new = R(:,:,t);
            X_new = X(:,:,t);

            %initialize loop variables and run "To" branch loop
            ix = 1;
            Branch_P = 0;
            Branch_Q = 0;
            for node = To_Side_new
                ix=ix+1;
                if ix <= length(To_Side_new)
                    ConMat_new(To_Side_new(ix),node)=1; %In the map, the second node is the sending node, and the first node is the receiving.
                    ConMat_new(node,To_Side_new(ix))=0; %We need to turn the opposite off, in case this map flips the power flow.
                    curLine = [To_Side_new(ix),node];
                else
                    ConMat_new(Loop_root,node)=1;
                    ConMat_new(node,Loop_root)=0;
                    curLine = [Loop_root,node];
                end
                % (3a) update the power flows
                Branch_P = Branch_P + Pnet(Pnet(:,1)==node,2); %moving along the branch, the load flow is simply the sum of the net loads because of our linear powerflow assumption.
                Branch_Q = Branch_Q + Qnet(Qnet(:,1)==node,2);
                Line_P_new(node) = Branch_P;
                Line_Q_new(node) = Branch_Q;

                % (4a) update R and X Matrices
                if ismember(curLine,line(:,1:2),'rows')
                    R_new(node,node) = line(ismember(line(:,1:2),curLine,'rows'),3);
                    X_new(node,node) = line(ismember(line(:,1:2),curLine,'rows'),4);
                elseif ismember([curLine(2) curLine(1)],line(:,1:2),'rows')
                    R_new(node,node) = line(ismember(line(:,1:2),[curLine(2) curLine(1)],'rows'),3);
                    X_new(node,node) = line(ismember(line(:,1:2),[curLine(2) curLine(1)],'rows'),4);
                else
                    disp('What is going on? Now my node map is pointing me to impossible lines.')
                end
            end

            %Repeat on the "From" branch
            ix = 1;
            Branch_P=0;
            Branch_Q=0;
            for node = From_Side_new
                ix=ix+1;
                if ix <= length(From_Side_new)
                    ConMat_new(From_Side_new(ix),node)=1; %In the map, the second node is the sending node, and the first node is the receiving.
                    ConMat_new(node,From_Side_new(ix))=0; %We need to turn the opposite off, in case this map flips the power flow.
                    curLine = [From_Side_new(ix),node];
                else
                    ConMat_new(Loop_root,node)=1;
                    ConMat_new(node,Loop_root)=0;
                    curLine = [Loop_root,node];
                end
                % (3b) update the power flows
                Branch_P = Branch_P + Pnet(Pnet(:,1)==node,2); %moving along the branch, the load flow is simply the sum of the net loads because of our linear powerflow assumption.
                Branch_Q = Branch_Q + Qnet(Qnet(:,1)==node,2);
                Line_P_new(node) = Branch_P;
                Line_Q_new(node) = Branch_Q;

                % (4b) update R and X Matrices
                if ismember(curLine,line(:,1:2),'rows')
                    R_new(node,node) = line(ismember(line(:,1:2),curLine,'rows'),3);
                    X_new(node,node) = line(ismember(line(:,1:2),curLine,'rows'),4);
                elseif ismember([curLine(2) curLine(1)],line(:,1:2),'rows')
                    R_new(node,node) = line(ismember(line(:,1:2),[curLine(2) curLine(1)],'rows'),3);
                    X_new(node,node) = line(ismember(line(:,1:2),[curLine(2) curLine(1)],'rows'),4);
                else
                    disp('What is going on? Now my node map is pointing me to impossible lines.')
                end
            end

            Tot_switches=Tot_switches+1;

            ConMat = ConMat_new;
            Line_P(:,t) = Line_P_new;
            Line_Q(:,t) = Line_Q_new;
            R(:,:,t) = R_new;
            X(:,:,t) = X_new;

            %disp(['Switch action #' num2str(Tot_switches)])
            %disp(['   Tie_in: ' num2str(Tie_in)])
            %disp(['   Sw_out: ' num2str(Sw_out)])
            %disp(['   Incremental Savings: ' num2str(Loss_Delta*S_base) ' kW'])
        end
    end
    %disp(['Switch actions taken: ' num2str(Tot_switches)])
    C(:,:,t) = ConMat;
    SwitchStates = SwitchStates_new;
    Final_Losses = Line_P(:,t)'*R(:,:,t)*Line_P(:,t) + Line_Q(:,t)'*R(:,:,t)*Line_Q(:,t);
    Tot_prct_red = (OG_Losses-Final_Losses)/OG_Losses;
end

function [Switch_Pair, loss] = Switched_Losses (To_lines, From_lines, Tie_Line, Pnet_To, Pnet_Fr, Qnet_To, Qnet_Fr, To_Branch, From_Branch, root_node)
    % Concatenate the Net Loads
    Pnet=[Pnet_To ; Pnet_Fr];
    Qnet=[Qnet_To ; Qnet_Fr];

    if size(To_lines,1)>0
        To_switches = To_lines(To_lines(:,6)==1,:); %Determine the switches on "to" branch:
        % Determine "to" losses
        To_Losses = switch_loss_branch(To_switches, To_Branch, From_Branch, Tie_Line, To_lines, From_lines, Pnet, Qnet, root_node); %Run this to examine losses around the network for all the switches on the "to" side of the tie-line
    else
        To_Losses = [];
    end
    
    if size(From_lines,1)>0
        From_switches = From_lines(From_lines(:,6)==1,:);
        From_Losses = switch_loss_branch(From_switches, From_Branch, To_Branch, Tie_Line, From_lines, To_lines, Pnet, Qnet, root_node); %Run this to examine losses around the network for all the switches on the "from" side of the tie-line
    else
        From_Losses = [];
    end
    
    Losses = [To_Losses ; From_Losses];
    if isempty(Losses)
        loss = inf;
        Switch_Pair = [0 0];
        display(['Closing switch between nodes ' num2str(Tie_Line(1)) ' and ' num2str(Tie_Line(2)) 'creates a loop'])
    else
        [loss, loss_indx] = min(Losses(:,3));
        Switch_Pair = Losses(loss_indx,1:2);
    end
    
end

function [Losses] = switch_loss_branch(branch_switches, sw_Branch, other_Branch, Tie_Line, sw_lines, other_lines, Pnet, Qnet, root_node)
    % - Branches are defined as the nodes that are passed through from their
    % leaf to the last node before the root.
    % - Lines are in reverse order of the branch list (root out to the leaf)
    % - The Tie_line is the line to be switched in.
    % - The switch designation indicates the branch in which closed  switches
    % are being opened to accomodate the tieline.
    % - The "other" designation is the branch from the root that does not include the
    % closed switches being evaluated.  
    
    %Initialize the output variable
    Losses = zeros(size(branch_switches,1),3); %Structure: Sw_node one, Sw_node 2, Losses
    lines = [Tie_Line ; sw_lines ; other_lines];
    
    for sw = 1:size(branch_switches,1)
        sw_pair = [branch_switches(sw,1) branch_switches(sw,2)];
        %Construct a new mapping and set of lines from the examined switch
        %position.
        %[sw_lines_new, other_lines_new, sw_Branch_new, other_Branch_new] = New_branch_map(sw_pair, sw_Branch, other_Branch, sw_lines, other_lines, Tie_Line);
        
        [sw_Branch_new, other_Branch_new] = post_switch_map(sw_pair, sw_Branch, other_Branch);
        
        %Now that the new branches are constructed around the switch
        sw_Losses = Branch_Losses (lines, Pnet, Qnet, sw_Branch_new, root_node); %Branch_Losses(lines, P_net_load, Q_net_load, branch_map, root_node)
        other_Losses = Branch_Losses (lines,  Pnet, Qnet, other_Branch_new, root_node); %Branch_Losses(lines, P_net_load, Q_net_load, branch_map, root_node)
        sw_Losses=sw_Losses + other_Losses; 
        
        % Let's record the switch losses and the switch
        Losses(sw,:) = [sw_pair sw_Losses];
    end
end

function [ Branch_losses ] = Branch_Losses(lines, P_net_load, Q_net_load, branch_map, root_node)
    Real_flow = 0;
    Reactive_flow = 0;
    Branch_losses = 0;
    for n = 1:length(branch_map)
        node = branch_map(n);
        if n ~= length(branch_map)
            %ID the line in question
            line_pair = [branch_map(n) branch_map(n+1)];
            % Find the Lines resistance
            line_info = find_lines(line_pair,lines);
            if length(line_info)<3
                disp(line_info)
            end
            
            R_line = line_info(3);
            % Calculate real and reactive power flows on the line as we
            % move down the branch
            Real_flow = Real_flow+P_net_load(P_net_load(:,1)==node,2);
            Reactive_flow = Reactive_flow+Q_net_load(Q_net_load(:,1)==node,2);
            % Calculate Branch_losses
            Branch_losses = Branch_losses + R_line * (Real_flow^2 + Reactive_flow^2);
        else
            line_pair = [branch_map(n) root_node];
            line_info = find_lines(line_pair,lines);
            R_line = line_info(3);
            
            Real_flow = Real_flow+P_net_load(P_net_load(:,1)==node,2);
            Reactive_flow = Reactive_flow+Q_net_load(Q_net_load(:,1)==node,2);
            
            Branch_losses = Branch_losses + R_line * (Real_flow^2 + Reactive_flow^2);
        end
    end
end

function [Pnet, Qnet, branch_lines] = Branch_Loads(Line_P,Line_Q,Branch_nodes, root_node, line)
    Pnet = zeros(length(Branch_nodes),2);
    Qnet =  zeros(length(Branch_nodes),2);
    branch_line_ns=zeros(length(Branch_nodes),2);
    for n = 1:length(Branch_nodes)
        if n == 1
            Pnet(n,1) = Branch_nodes(n);
            Pnet(n,2) = Line_P(Branch_nodes(n),:);
            Qnet(n,1) = Branch_nodes(n);
            Qnet(n,2) = Line_Q(Branch_nodes(n),:);
            
        else
            Pnet(n,1) = Branch_nodes(n);
            Pnet(n,2) = Line_P(Branch_nodes(n),:)-Line_P(Branch_nodes(n-1),:);
            Qnet(n,1) = Branch_nodes(n);
            Qnet(n,2) = Line_Q(Branch_nodes(n),:)-Line_Q(Branch_nodes(n-1),:);
        end
        if n == length(Branch_nodes)
            branch_line_ns(n,:)=[root_node Branch_nodes(n)];
        else
            branch_line_ns(n,:)=[Branch_nodes(n+1) Branch_nodes(n)];
        end
    end
    branch_lines = find_lines(branch_line_ns, line);
end

function [root_map] = map_root(Starting_Node,Connectivity_Matrix)
        root_map = Starting_Node; %Initialize a double that will contain all nodes between the To_node of the tie_line and the network root (node 1).
        curNode = Starting_Node; %Establish the current node for the while loop
        while curNode ~= 1 %While the current node is not the root of the network
            % examine the row index of the current nodes column in C that
            % indicates the 
            curNode = find(Connectivity_Matrix(:,curNode));
            root_map(end+1)=curNode;
        end
end

function [Sw_map, ot_map] = post_switch_map(sw_pair, Switch_side_map, other_side_map)
    Switch_location = ismember(Switch_side_map,sw_pair);
    if sum(Switch_location)==0
        return
    end
    Sw_map = Switch_side_map;
    ot_map = other_side_map;
    ix = 1;
    while ~Switch_location(ix)
        ot_map = [ Sw_map(1) ot_map ];
        Sw_map = Sw_map(2:end);
        ix=ix+1;
    end
    ot_map = [ Sw_map(1) ot_map ];
    Sw_map = Sw_map(2:end);
end

function [line_info] = find_lines(node_pair,lines)
    num_lines = size(node_pair,1);
    line_info = zeros(num_lines,6);
    for l = 1:num_lines
       if ~ismember(node_pair(l,:), lines(:,1:2),'rows')
            node_pair(l,:) = [node_pair(l,2) node_pair(l,1)];
        end
        line_info(l,:) = lines(ismember(lines(:,1:2),node_pair(l,:),'rows'),:);
    end

end