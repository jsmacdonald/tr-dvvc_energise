function [Line_P, Line_Q, Line_V2, Pstar_PV, Qstar_PV, optval, status] = Loss_Min_OPF_qp(Load_P, Load_Q, timesteps, PV_P_t, PV_Q_t, PV_Cap, PV_Loc, Line_Cap, Num_Buses, R, X, C, lines_180 )

% From CVX:
% %         variable Line_P(Num_Buses,timesteps) 
% %         variable Line_Q(Num_Buses,timesteps)
% %         variable Line_V2(Num_Buses,timesteps) 
% %         variable Pstar_PV(Num_Buses,timesteps) nonnegative 
% %         variable Qstar_PV(Num_Buses,timesteps)

%Objective Function
%fun = @(vars)Optim_Obj(vars, R, timesteps, Num_Buses);
[H, f] = Obj_quad(R, timesteps, Num_Buses);

%Initial Condition
vars0 = zeros(5*Num_Buses, timesteps);
vars0(2*Num_Buses+1:3*Num_Buses, :) = 1;
vars0(3*Num_Buses+1:4*Num_Buses, :) = PV_P_t;
vars0(4*Num_Buses+1:5*Num_Buses, :) = PV_Q_t;

vars0=vars0(:); %vector form

%Linear constraints
[A, b, Aeq, beq] = powerflow_linear(R, X, C, timesteps, Num_Buses, Load_P, Load_Q, PV_Loc);

%Linearizing the quadratic apparent power capacity constraints
sections180 = lines_180; %The number of linear constraints made from 180 degrees of the circle capacity constraints
[Alin,Blin] = linearized_cap_constraints(sections180, Num_Buses, timesteps, PV_Cap, Line_Cap);

A= [A;Alin];
b = [b; Blin];

%Bounding Constraints
lb = zeros(5*Num_Buses, timesteps);
lb(1:2*Num_Buses,:) = -inf;
lb(2*Num_Buses+1:3*Num_Buses,:) = -inf;%0.95^2;%
lb(3*Num_Buses+1:4*Num_Buses,:) = 0;
lb(4*Num_Buses+1:5*Num_Buses,:) = -inf;

lb = lb(:); %vector form

ub = zeros(5*Num_Buses, timesteps);
ub(1:2*Num_Buses,:) = inf;
ub(2*Num_Buses+1:3*Num_Buses,:) = inf;%1.05^2;%
ub(3*Num_Buses+1:4*Num_Buses,:) = PV_P_t;
ub(4*Num_Buses+1:5*Num_Buses,:) = inf;

ub = ub(:); %vector form

%Voltage at sub:

%Nonlinear Constraints (
% Line_Cap =[]; %Will need to add a line thermal capacity to inputs if that ever comes up.
% nonlcon = @(vars)nonlinear_cons(vars, PV_Cap, Line_Cap, timesteps, Num_Buses);

%Specify fmincon options
% options = optimoptions('fmincon','Display','iter','Algorithm','sqp','SpecifyObjectiveGradient',true,'SpecifyConstraintGradient',true,'OptimalityTolerance',5e-4);
%options = optimoptions('fmincon','Display','iter','Algorithm','interior-point','SpecifyObjectiveGradient',true,'SpecifyConstraintGradient',true);


%Run Optimization
[vars, optval, exitflag, output]= quadprog(H,f,A,b,Aeq,beq,lb,ub,vars0);
%[vars, optval, exitflag, output]= quadprog(H,f,A,b,Aeq,beq,lb,ub,vars0,options);

%Break out the results

%initialize
Line_P = zeros(Num_Buses,timesteps);
Line_Q = zeros(Num_Buses,timesteps);
Line_V2 = zeros(Num_Buses,timesteps);
Pstar_PV = zeros(Num_Buses,timesteps);
Qstar_PV = zeros(Num_Buses,timesteps);

% % Loop through t to backfill
for t = 1:timesteps
    basepoint = (t-1)*5*Num_Buses;
    Line_P(:,t) = vars(basepoint+1:basepoint+Num_Buses,1);
    Line_Q(:,t) = vars((basepoint+1*Num_Buses+1):basepoint+2*Num_Buses,1);
    Line_V2(:,t) = vars((basepoint+2*Num_Buses+1):basepoint+3*Num_Buses,1);
    Pstar_PV(:,t) = vars((basepoint+3*Num_Buses+1):basepoint+4*Num_Buses,1);
    Qstar_PV(:,t) = vars((basepoint+4*Num_Buses+1):basepoint+5*Num_Buses,1);
end

% Check the Exit Flag
if exitflag == 1
    status = 'Solved';
elseif exitflag == 0
    status = 'Nonconvergence';
else
    status = 'No Solution';
end

end