function [H, f] = Obj_quad(R, timesteps, Num_Buses)


% mapping the cvx variables to locations in the fmincon optimization variables: vars
%Line_P = vars(1:Num_Buses,:);
%Line_Q = vars((1*Num_Buses+1):2*Num_Buses,:);
%Line_V2 = vars((2*Num_Buses+1):3*Num_Buses,:);
%Pstar_PV = vars((3*Num_Buses+1):4*Num_Buses,:);
%Qstar_PV = vars((4*Num_Buses+1):5*Num_Buses,:);

f = []; %no linear portion of the objective

%Equality constraints: Aeq*x-beq = 0
H_it = [];
Z = zeros(Num_Buses,Num_Buses);

for t = 1:timesteps
    f = [f; 1 ; zeros(Num_Buses*5-1,1)];% Add in the real power through the feeder head.
    %first we make a Aeq that is only for this time point.
    H_it = blkdiag(H_it, 2*R(:,:,t), 2*R(:,:,t), Z, Z, Z);
end

H = H_it;
end