function [A,B] = linearized_cap_constraints(sections180, Num_Buses, timesteps, PV_Cap, Line_Cap)

% mapping the cvx variables to locations in the fmincon optimization variables: vars
%Line_P = vars(1:Num_Buses,:);
%Line_Q = vars((1*Num_Buses+1):2*Num_Buses,:);
%Line_V2 = vars((2*Num_Buses+1):3*Num_Buses,:);
%Pstar_PV = vars((3*Num_Buses+1):4*Num_Buses,:);
%Qstar_PV = vars((4*Num_Buses+1):5*Num_Buses,:);

Num_Cap_Cons = sum(PV_Cap>0)+sum(Line_Cap>0);

A = zeros(sections180*2*Num_Cap_Cons*timesteps,5*Num_Buses*timesteps);
B = zeros(sections180*2*Num_Cap_Cons*timesteps,1);

delDeg = 180/sections180;

Caps = {Line_Cap PV_Cap;...
    [1 2] [4 5]};

rowCon = 0;
for whichCap = 1:2
    if ~isempty(Caps{1,whichCap})
        for n = 1:Num_Buses
            cap = Caps{1,whichCap}(n);
            if cap > 0
                indexP = (Caps{2,whichCap}(1)-1)*Num_Buses +n;
                indexQ = (Caps{2,whichCap}(2)-1)*Num_Buses +n;
                
                
                for theta = 0:delDeg:180-delDeg
                    m = (sind(theta+delDeg) - sind(theta))/(cosd(theta+delDeg) - cosd(theta));
                    b = cap * (sind(theta) - cosd(theta)*m);
                    for t = 1:timesteps
                        basepoint = (t-1)*5*Num_Buses;
                        a = zeros(1,5*Num_Buses*timesteps);
                        a(basepoint+indexQ) = 1;
                        a(basepoint+indexP) = -m;
                        rowCon = rowCon+1;
                        A(rowCon,:) = a;
                        B(rowCon,1) = b;
                    end
                end
                
                for theta = 0:-delDeg:-180+delDeg
                    m = (sind(theta-delDeg) - sind(theta))/(cosd(theta-delDeg) - cosd(theta));
                    b = cap * (sind(theta) - cosd(theta)*m);
                    for t = 1:timesteps
                        basepoint = (t-1)*5*Num_Buses;
                        a = zeros(1,5*Num_Buses*timesteps);
                        a(basepoint+indexQ) = -1;
                        a(basepoint+indexP) = m;
                        rowCon = rowCon+1;
                        A(rowCon,:) = a;
                        B(rowCon,1) = -b;
                    end
                end
                
            end
        end
    end
end
%disp(rowCon)
%disp(Num_Cap_Cons)