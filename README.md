tr-dvvc_energise

Author: Jason MacDonald

This repository contains two distinct algorithms developed by Lawrence Berkeley National Laboratory 
for an ENERGISE Project entitled "Integration of a DER Management System in Riverside".

One algorithm is a topology reconfiguration algorithm that seeks to minimize losses in a distribution feeder
The other algorithm (presented in a scenario-driven simulation) is a distributed, model-free control 
approach that allows DER to minimize any convex objective, called Extremum Seeking.  This algorithm is applied to 
voltage control and phase balancing in the code in the repository.

Each algorithm has it's own readme and license file.  Both are under modified BSD-3 open source licenses.