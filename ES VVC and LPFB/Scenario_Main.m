clear
clc
close all

%% Scenario analysis for Extremum Seeking
% Author: Jason MacDonald
% Date: July 25th, 2018

%% Prepare input data:
home = pwd; %returns a string for the path of the base folder in which the .m file is housed.
if ispc %This should identify the appropriate pathseparator for the operating system so that it will run on a windows, mac, or linux machine.
    pathseparator = '\';
else
    pathseparator = '/';
end
genpathstr=strcat(home); %path string to the GEN folder
feederpathstr=strcat(home,pathseparator, 'Networks', pathseparator); %path string to the Networks folder
path(path,genpath(genpathstr));

%% Choose ES Objective
Objectives = {'VReg', 'VBal', 'INeutBal', 'losses'};
objective=Objectives{1};

%% Load feeder

%networkname = '05node_fullphase_radial';
networkname = 'ieee_34node';

feedername = [networkname '.txt'];

[network1] = network_mapper_function(feederpathstr, feedername);

%% Feeder paramaters

nnode = network1.nodes.nnode;
nline = network1.lines.nline;

%% Load parameters
load('loading.mat')
%total_time = 60*60*24; %seconds in a day

% Identify the average loading for PV sizing
S_tot = zeros(1,1440);
Num_Buses=length(loading);
for n=1:Num_Buses
    S_tot = S_tot+sqrt(loading(n).P.^2+loading(n).Q.^2);
end
Avg_Loading_Base=mean(S_tot);
Max_Loading=max(S_tot); %Don't think we need this, but may be useful point for comparison

dt_out = 0.1; %The Granularity of output timeseries

% Set ZIP load parameters
network1.loads.aPQ = 1.00*ones(3,nnode).*network1.nodes.PH;
network1.loads.aI = 0.0*ones(3,nnode).*network1.nodes.PH;
network1.loads.aZ = 0.00*ones(3,nnode).*network1.nodes.PH;

network1.loads.spu = 1.0*network1.loads.spu;

%% Prepare PV data and Scenarios

% PV Scenarios (a total of 54)
PV_Nodes_Scens = {2, 18, [2,10], [10,18], [18,33], [18, 18], ...
    [2, 10, 18, 33], [2,2,10,18], [10, 18, 18, 33], [2, 18, 33, 33]...
    [18, 18, 33, 33], [2, 2, 10, 10]};

phase10scens = {'abc'};

phase20scens = { {'abc', 'abc'}, {'ac', 'bc'}, {'ab', 'bc'}, {'ab', 'ac'}};

phase40scens = { {'abc', 'abc', 'abc' ,'abc'}, {'a', 'b', 'c' ,'abc'}, ...
    {'abc', 'c', 'b' ,'a'}, {'ab', 'bc', 'ac' ,'abc'}, ...
    {'bc', 'ac', 'ab' ,'abc'}, {'ac', 'ac', 'bc' ,'c'}};

scen_idx = 1;
PV_Scenarios =  {};
for n = 1:length(PV_Nodes_Scens)
    nodes = PV_Nodes_Scens{n};
    if length(nodes) == 1
        PV_Scenarios{scen_idx} = {nodes; phase10scens};
        scen_idx = scen_idx+1;
    elseif length(nodes) == 2
        for m = 1:length(phase20scens)
            PV_Scenarios{scen_idx} = {nodes; phase20scens{m}};
            scen_idx = scen_idx+1;
        end
    elseif length(nodes)== 4
        for m = 1:length(phase40scens)
            PV_Scenarios{scen_idx} = {nodes; phase40scens{m}};
            scen_idx = scen_idx+1;
        end
    end
end
    
%% Capacitor parameters

network1.caps.cappu = 1.0*network1.caps.cappu;

%% Controller parameters

network1.cons.wmaxpu = 0.10*network1.cons.wmaxpu;


%FullDayResults_headings = {'Total_switches' ; 'Original_Losses' ; 'Final_Losses' ; 'Total_Percent_Reduction' };



%% Iterate Across Scenarios
scen_num = 0;%19;%
for day_type = {'Sunny' 'Rainy'}%{'Sunny'}%
    for PV_locs = PV_Scenarios%{scen_num}
        PV_Locs = PV_locs{1};
        disp(PV_Locs{1,1})
        scen_num =  scen_num+1;
        if scen_num > 0
        PV_Caps = ones(1,length(PV_Locs{1}))*Avg_Loading_Base*0.1;
        %Let's get the per unit capacities and generation
        [PVCap_matrix] = PVCap_matrix_creator(PV_Caps, PV_Locs, network1, day_type);
        
        %Reconfigure Static control parameters
        PVGensLogical = PVCap_matrix>0;
        network1.cons.wmaxpu = PVCap_matrix;
        network1.cons.hpfes = PVGensLogical*0.5;
        network1.cons.lpfes = PVGensLogical*0.05;
        network1.cons.kintes = -0.75*PVGensLogical;
        Amplitude = 0.001; %probing amplitude
        %Slow down the probing frequencies
        feses = double(PVGensLogical);
        fes = 0.5;
        for row = 1:size(PVCap_matrix,1)
            for col = 1:size(PVCap_matrix,2)
                feses(row,col) = fes*feses(row,col);
                if feses(row,col)~=0
                    fes = fes - 0.005;
                end
            end
        end
        network1.cons.fes = feses;
        network1.cons.wpu = zeros(3,nnode);% This creates an initial condition of the 
        
        %% Loop across time periods
        Loop_time = 60*60*2;%60*10;%60*30;
        %Sim_time = 60*60*24-60;
        for time = 16*60*60%0%[8*60*60, 12*60*60, 21*10*10]%0:Loop_time:Sim_time
            starttime = time;
            endtime = time+Loop_time;

            %Per unit loading
            [PLoad_matrix, QLoad_matrix] = Load_matrix_creator(network1, starttime, endtime, dt_out);
            [PPV_matrix, QPV_matrix] = PV_matrix_creator(PV_Caps, PV_Locs, network1, dt_out, starttime, endtime, day_type);



            disp('Starting Simulation')
            ESenabled = true;%false;%true;
            %%Run the Simulation
            [u, v, J, voltage, sendingPower, receivingPower, network1] = ES_Simulation(network1, PPV_matrix, QPV_matrix, PLoad_matrix, QLoad_matrix, dt_out, starttime, endtime, objective, Amplitude, ESenabled);


            Resultfilename = strcat('Results2/ScenResults_',objective,'_', day_type, '_', num2str(starttime), '-', num2str(endtime), '_', num2str(scen_num), '-ES', num2str(ESenabled), '.mat');
            save(Resultfilename{1}, 'PV_Locs', 'network1', 'u', 'v', 'J', 'voltage', 'sendingPower', 'receivingPower')
        end
        end
    end
end

