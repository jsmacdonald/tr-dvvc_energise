function [Jk] = compute_objective_voltage_magnitude_balance(network1,Vk,Ik,STXk,SRXk,ik,sk)

base = network1.base;
nodes = network1.nodes;
lines = network1.lines;
loads = network1.loads;
caps = network1.caps;
cons = network1.cons;

nnode = nodes.nnode;

Vkmag2 = abs(Vk).^2;

for k1 = 1:nnode
    
    if strcmp('ab',nodes.phases(k1))
        Vbal(k1) = (Vkmag2(1,k1) - Vkmag2(2,k1))^2;
    end
    if strcmp('bc',nodes.phases(k1))
        Vbal(k1) = (Vkmag2(2,k1) - Vkmag2(3,k1))^2; 
    end
    if strcmp('ac',nodes.phases(k1))
        Vbal(k1) = (Vkmag2(1,k1) - Vkmag2(3,k1))^2;
    end
    if strcmp('abc',nodes.phases(k1))
        Vbal(k1) = (Vkmag2(1,k1) - Vkmag2(2,k1))^2 + ...
            (Vkmag2(2,k1) - Vkmag2(3,k1))^2 + ...
            (Vkmag2(3,k1) - Vkmag2(1,k1))^2;
    end
    
end

Jk = sum(Vbal,2);

end