function [PVCap_matrix] = PVCap_matrix_creator(PV_Caps, PV_Locs, network, day_type)

%PV is the struct that UCR created
%PV_Caps is a vector of maximum capacities for each PV resource (it will be
% 10% of average loading for all scenarios run in this experiment)
%PV_Locs is a cell, with the top row being integer values indicating the
% appropriate node of the PV generator, and the second row indicating with
% an string the phases on which the generators capacity is acting.
%network is the output from the network mapper function
%nodemap is the mapping I created between the IEEE node designations and
% the UCR node designations
%dt_out is the desired delta time between data points (seconds)
%dt_in is the desired delta time of the input data (seconds)
%S_base is the per unit base apparent power magnitude

load('PV.mat')
%from this I'll get PV struct and the nodemapIEEE2UCR

%the outputs are 3 dimensional matrices (phases, nodes from network, time) for load.  
PV_Cap_in = max(sqrt(PV.P_Sunny.^2+PV.Q_Sunny.^2));
if strcmp(day_type,'Rainy')
    P_in = PV.P_Rainy/PV_Cap_in;
    Q_in = PV.Q_Rainy/PV_Cap_in;
else
    P_in = PV.P_Sunny/PV_Cap_in;
    Q_in = PV.Q_Sunny/PV_Cap_in;
end


nnode_out = network.nodes.nnode; %The number of nodes recorded in the output
PVnodes_in = size(PV_Locs{1,1}, 2); %The number of nodes at which PV will act.

nodelist = network.nodes.nodelist; %is a cell with strings in each cell with the node numbers;
S_Base = network.base.Sbase/1000; %base in kVAR

PVCap_matrix = zeros(3,nnode_out);

for n = 1:PVnodes_in
    Node = PV_Locs{1,1}(1,n);
    numphases = length(PV_Locs{2,1}{1,n});
    PV_Cap = PV_Caps(n)/numphases;
    for Phase = PV_Locs{2,1}{1,n}
        %We need to identify the correct node indx for the network model to apply
        %the time series to.
        Node_IEEE = nodemapIEEE2UCR(nodemapIEEE2UCR(:,2)==Node,1);
        Node_idx = strcmp(nodelist, num2str(Node_IEEE));

        % Let's put the timeseries in the right spaces
        if strcmp(Phase,'a')
            PVCap_matrix(1,Node_idx) = PVCap_matrix(1,Node_idx) + PV_Cap/S_Base;
        elseif strcmp(Phase,'b')
            PVCap_matrix(2,Node_idx) = PVCap_matrix(2,Node_idx) + PV_Cap/S_Base;
        elseif strcmp(Phase,'c')
            PVCap_matrix(3,Node_idx) = PVCap_matrix(3,Node_idx) + PV_Cap/S_Base;
        else
            disp(strcat('Bad Phase ID - Node ', num2str(Node), ', Phase: ', Phase))
        end
    end
end