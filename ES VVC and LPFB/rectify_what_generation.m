function [uhatk,vhatk] = rectify_what_generation(tk,uk,vk,uhatk,vhatk,fes,ak)

% if uk >= 0 && mod(2*pi*fes*tk,pi/2) ~= 0
%     hk = vhatk - uhatk*tan(2*pi*fes*tk);
% %     ck = a0*(cos(2*pi*fes*tk) + 1j*sin(2*pi*fes*tk));
%     ck = ak*exp(1j*2*pi*fes*tk);
%     whatk = 1j*hk - ck;
%     uhatk = real(whatk);
%     vhatk = imag(whatk);
% end

wk = uk + 1j*vk;
whatk = uhatk + 1j*vhatk;
ck = ak*exp(1j*2*pi*fes*tk);

if uhatk <= 0 && uk >= 0
    
    h = vhatk + ak*sin(2*pi*fes*tk);
    whatk = (0 + 1j*h) - ck;
    
elseif uhatk >= 0 && uk >= 0
    
    h = vhatk + ak*sin(2*pi*fes*tk);
    whatk = (0 + 1j*h) - ck;
    
end

uhatk = real(whatk);
vhatk = imag(whatk);

end

