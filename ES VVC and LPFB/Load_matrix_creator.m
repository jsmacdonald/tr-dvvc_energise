function [PLoad_matrix, QLoad_matrix] = Load_matrix_creator(network, starttime, endtime, dt_out)

%loading is the struct that UCR created
%network is the output from the network mapper function
%nodemap is the mapping I created between the IEEE node designations and
% the UCR node designations
%dt_out is the desired delta time between data points (seconds)
%dt_in is the desired delta time of the input data (seconds)
% dt_out must be less than dt_in

%the outputs are 3 dimensional matrices (phases, nodes from network, time) for load.  

load('loading.mat')
% Gives us the 'loading' struct, dt_in, and the nodemapIEEE2UCR

nnode_out = network.nodes.nnode; %The number of nodes recorded in the output
ts_in = length(loading); %The number of input time series.

nodelist = network.nodes.nodelist; %is a cell with strings in each cell with the node numbers;
S_Base = network.base.Sbase/1000; %base in kVAR

%Let's identify the start and end time indices in the original time series:
startindex = floor(starttime/dt_in)+1;
endindex = ceil(endtime/dt_in)+1;
start_adj = (startindex-1)*dt_in; %the +/- 1 allows for index = 1 be time =0
end_adj = (endindex-1)*dt_in; %the +/- 1 allows for index = 1 be time =0


%time and indices
indices = startindex:endindex;
t_in = start_adj:dt_in:end_adj;
t_out = start_adj:dt_out:end_adj;

ts_out_length = length(t_out); %The length of the input time series

%Initialize output matrices
PLoad_matrix = zeros(3,nnode_out,ts_out_length);
QLoad_matrix = zeros(3,nnode_out,ts_out_length);

for n = 1:ts_in
    % Read the values in the UCR label
    Label = loading(n).Label{1};
    Phase = Label(end);
    Node = str2num(Label(1:length(Label)-2));
    
    %need input time series the same length as t_in
    P_in = loading(n).P(indices);
    Q_in = loading(n).Q(indices);
    
    %interpolate the difference between dt_out and dt_in
    P_ts = interp1(t_in,P_in,t_out);
    Q_ts = interp1(t_in,Q_in,t_out);
    
    %We need to identify the correct node indx for the network model to apply
    %the time series to.
    Node_IEEE = nodemapIEEE2UCR(nodemapIEEE2UCR(:,2)==Node,1);
    Node_idx = strcmp(nodelist, num2str(Node_IEEE));
    
    % Let's put the timeseries in the right spaces
    if strcmp(Phase,'a')
        PLoad_matrix(1,Node_idx,:) = P_ts/S_Base;
        QLoad_matrix(1,Node_idx,:) = Q_ts/S_Base;
    elseif strcmp(Phase,'b')
        PLoad_matrix(2,Node_idx,:) = P_ts/S_Base;
        QLoad_matrix(2,Node_idx,:) = Q_ts/S_Base;
    elseif strcmp(Phase,'c')
        PLoad_matrix(3,Node_idx,:) = P_ts/S_Base;
        QLoad_matrix(3,Node_idx,:) = Q_ts/S_Base;
    end
end