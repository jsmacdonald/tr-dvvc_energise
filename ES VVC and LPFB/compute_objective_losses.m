function [Jk] = compute_objective_losses(network1,Vk,Ik,STXk,SRXk,ik,sk)

base = network1.base;
nodes = network1.nodes;
lines = network1.lines;
loads = network1.loads;
caps = network1.caps;
cons = network1.cons;

nline = lines.nline;

Lmn = zeros(3,nline);
for k1 = 1:nline
    
    txnode = lines.TXnum(k1);
    rxnode = lines.RXnum(k1);
    
    if strcmp('a',lines.phases(k1))
        Lmn(1,k1) = (Vk(1,txnode) - Vk(1,rxnode))*conj(Ik(1,k1));
    end
    if strcmp('b',lines.phases(k1))
        Lmn(2,k1) = (Vk(2,txnode) - Vk(2,rxnode))*conj(Ik(2,k1));
    end
    if strcmp('c',lines.phases(k1))
        Lmn(3,k1) = (Vk(3,txnode) - Vk(3,rxnode))*conj(Ik(3,k1));
    end   
    if strcmp('ab',lines.phases(k1))
        Lmn(1:2,k1) = (Vk(1:2,txnode) - Vk(1:2,rxnode)).*conj(Ik(1:2,k1));
    end
    if strcmp('bc',lines.phases(k1))
        Lmn(2:3,k1) = (Vk(2:3,txnode) - Vk(2:3,rxnode)).*conj(Ik(2:3,k1));
    end
    if strcmp('ac',lines.phases(k1))
        Lmn([1 3],k1) = (Vk([1 3],txnode) - Vk([1 3],rxnode)).*conj(Ik([1 3],k1));
    end
    if strcmp('abc',network1.nodes.phases(k1))
        Lmn(:,k1) = (Vk(:,txnode) - Vk(:,rxnode)).*conj(Ik(:,k1));
    end
    
end

% Lmn = zeros(3,nline);
% for k1 = 1:nline
%     
%     Lmn(:,k1) = STXk(:,k1) - SRXk(:,k1);
%     
% end
% 
% Lmn

Jk = sum(sum(Lmn.*conj(Lmn)));

end