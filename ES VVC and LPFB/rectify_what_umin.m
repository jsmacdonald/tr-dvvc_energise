function [uhatk,vhatk] = rectify_what_umin(tk,uk,vk,uhatk,vhatk,fes,ak,umin)

%
if uhatk <= umin + ak
    
%     disp('umin')
    
%     uhatk
%     
%     umin + ak
    
    gamma = (umin + ak)/uhatk;
    uhatk = gamma*uhatk;
    vhatk = gamma*vhatk;    
    
    whatk = uhatk + 1j*vhatk;
    
end



% ADVANCED
% if uk <= umin && mod(2*pi*fes*tk,pi/2) ~= 0
%     hk = vhatk - uhatk*tan(2*pi*fes*tk);
% %     ck = a0*(cos(2*pi*fes*tk) + 1j*sin(2*pi*fes*tk));
%     ck = ak*exp(1j*2*pi*fes*tk);
%     whatk = 1j*hk - ck;
%     uhatk = real(whatk);
%     vhatk = imag(whatk);
% end


% MORE ADVANCED
% wk = uk + 1j*vk;
% whatk = uhatk + 1j*vhatk;
% ck = ak*exp(1j*2*pi*fes*tk);
% 
% if uhatk >= umin && uk <= umin && mod(2*pi*fes*tk,pi/2) ~= 0
%     
%     gamma = (umin - uhatk)/(ak*cos(2*pi*fes*tk));
%     vmin = vhatk + gamma*ak*sin(2*pi*fes*tk);
%     whatk = (umin + 1j*vmin) - ck;
%     
%     
% elseif uhatk <= umin && uk <= umin && mod(2*pi*fes*tk,pi/2) ~= 0
%     
%     gamma = umin/uhatk;
%     vmin = gamma*vhatk;
%     whatk = (umin + 1j*vmin) - ck;
%     
% elseif uhatk <= umin && uk <= umin && mod(2*pi*fes*tk,pi/2) == 0
%     
%     gamma = umin/uhatk;
%     vmin = gamma*vhatk;
%     whatk = (umin + 1j*vmin);
%     
%     
% end
% 
% uhatk = real(whatk);
% vhatk = imag(whatk);

end

