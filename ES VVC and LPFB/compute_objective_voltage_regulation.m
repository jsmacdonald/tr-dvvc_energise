function [Jk] = compute_objective_voltage_regulation(network1,Vk1,Ik,STXk,SRXk,ik,sk)

nnode = network1.nodes.nnode;

gamma = 20;

Vk = abs(Vk1);

Vreg = zeros(3,nnode);
for k1 = 2:nnode
    
    if strcmp('a',network1.nodes.phases(k1))
        Vreg(1,k1) = exp(gamma*(0.95 - Vk(1,k1))) + exp(gamma*(Vk(1,k1) - 1.05));
    end
    if strcmp('b',network1.nodes.phases(k1))
        Vreg(2,k1) = exp(gamma*(0.95 - Vk(2,k1))) + exp(gamma*(Vk(2,k1) - 1.05));
    end
    if strcmp('c',network1.nodes.phases(k1))
        Vreg(3,k1) = exp(gamma*(0.95 - Vk(3,k1))) + exp(gamma*(Vk(3,k1) - 1.05));
    end   
    if strcmp('ab',network1.nodes.phases(k1))
        Vreg(1:2,k1) = exp(gamma*(0.95 - Vk(1:2,k1))) + exp(gamma*(Vk(1:2,k1) - 1.05));
    end
    if strcmp('bc',network1.nodes.phases(k1))
        Vreg(2:3,k1) = exp(gamma*(0.95 - Vk(2:3,k1))) + exp(gamma*(Vk(2:3,k1) - 1.05)); 
    end
    if strcmp('ac',network1.nodes.phases(k1))
        Vreg([1 3],k1) = exp(gamma*(0.95 - Vk([1 3],k1))) + exp(gamma*(Vk([1 3],k1) - 1.05));
    end
    if strcmp('abc',network1.nodes.phases(k1))
        Vreg(:,k1) = exp(gamma*(0.95 - Vk(:,k1))) + exp(gamma*(Vk(:,k1) - 1.05));
    end
    
end

Vreg = Vreg/gamma;

Jk = sum(sum(Vreg));

end