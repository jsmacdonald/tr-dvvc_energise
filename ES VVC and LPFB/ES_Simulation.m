% Author: Michael Sankur
% Date: July 25th, 2018
function [u,v,J,voltage, sendingPower, receivingPower, network1] = ES_Simulation(network1, PPV_matrix, QPV_matrix, PLoad_Matrix, QLoad_Matrix, dt, starttime, endtime, objective, Amplitude, ESenabled)

%% Feeder paramaters

nnode = network1.nodes.nnode;
nline = network1.lines.nline;

%% Initialize network power flow
Vnom = [1;
    1*exp(1i*240*pi/180);
    1*exp(1i*120*pi/180)];
% sim.Vnom = Vnom(1);

slacknode = 1;
% sim.slacknode = 1;

% set up loads on the network.
[network1] = network_loader(network1, PLoad_Matrix, QLoad_Matrix, 1);

[~, VNR0, INR0, STXNR0, SRXNR0, iNR0, sNR0] = NR3(network1,slacknode,Vnom,[],[]);

% for kn = 1:nnode
%     if kn ~= slacknode
%         intemp = network1.nodes.inmat(:,kn);
%         intemp = intemp(
%         sk(:,kn) = sum(SRXNRk(:,network1.nodes.inmat(:,kn))) - sum(STXNRk(:,network1.nodes.outmat(:,kn)));
%     end            
% end

%% Simulation Setup
starttime_adj = floor(starttime/dt)*dt;
endtime_adj = ceil(endtime/dt)*dt;

time = starttime_adj:dt:endtime_adj;
if length(time)>size(PLoad_Matrix,3)
    disp('We''ve got a problem with the length of the time vector')
end

% Initialize vectors
MD = zeros(3,length(time));
ED = zeros(3,length(time));
AD = zeros(3,length(time));
PD = zeros(3,length(time));

J = zeros(1,length(time)); % objective function
Jdot = zeros(1,length(time)); % time derivative of objective function

rhou = zeros(3,nnode,length(time)); % value after highpass (washout) filter
sigmau = zeros(3,nnode,length(time)); % value after demodulation
xiu = zeros(3,nnode,length(time)); % value after lowpass filter

uhat = zeros(3,nnode,length(time)); % value after integration
u = zeros(3,nnode,length(time));  % control outputs of ES control (position)
au = zeros(3,nnode,length(time));  % adaptive probing amplitude

rhov = zeros(3,nnode,length(time)); % value after highpass (washout) filter
sigmav = zeros(3,nnode,length(time)); % value after demodulation
xiv = zeros(3,nnode,length(time)); % value after lowpass filter

vhat = zeros(3,nnode,length(time)); % value after integration
v = zeros(3,nnode,length(time));
av = zeros(3,nnode,length(time));  % adaptive probing amplitude
% e = zeros(2,length(time),nc);  % LPF of objective

voltage = zeros(3,nnode,length(time));
sendingPower = zeros(3,nline,length(time));
receivingPower = zeros(3,nline,length(time));

% initial conditions
u(:,:,1) = 0;
v(:,:,1) = 0;
MD(:,1) = abs(VNR0(:,4)) - abs(VNR0(:,6));
ED(:,1) = abs(VNR0(:,4)).^2 - abs(VNR0(:,6)).^2;
AD(:,1) = angle(VNR0(:,4)) - angle(VNR0(:,6));
PD(:,1) = abs(VNR0(:,4)- VNR0(:,6));

% system state vectors
ii = zeros(3,nnode,length(time));
ss = zeros(3,nnode,length(time));

% calculate initial objective function
% s0 = network1.loads.spu.*(network1.loads.aPQ + network1.loads.aI.*abs(VNR0) + network1.loads.aZ.*abs(VNR0).^2) - 1j*network1.caps.cappu;
% s0(network1.nodes.PH == 0) = 0;
ss(:,:,1) = sNR0;

% i0 = conj(s0./VNR0);
% ik(network1.nodes.PH == 0) = 0;        
ii(:,:,1) = iNR0;

J0 = compute_objective(objective, network1,VNR0,INR0,STXNR0,SRXNR0,iNR0,sNR0);
J(1) = J0;



% setup parameters for the ES controller
a0 = Amplitude %Amplitude of the probe, default was 0.001
wmaxpu = network1.cons.wmaxpu
fes = network1.cons.fes
hpfes = network1.cons.hpfes
lpfes = network1.cons.lpfes
kintes = network1.cons.kintes

tic

for kt = 1:length(time)
    
    % display time
    if mod(kt-1,1000) == 0
        disp(['kt = ' num2str(kt) ' | time = ' num2str(time(kt))])
    end

    % start ES control on second timestep
    if kt >= 2
        
        % control from previous timestep
        if ESenabled
            wk = 1*u(:,:,kt-1) + 1j*v(:,:,kt-1);
        else
            wk = PPV_matrix(:,:,kt-1)+1j*QPV_matrix(:,:,kt-1);
        end
        network1.cons.wpu = wk;
        
        % XXX
        % input time varying load
        [network1] = network_loader(network1, PLoad_Matrix, QLoad_Matrix, kt);
        
        % solve power flow
        [~, Vk, Ik, STXk, SRXk, ik, sk, iterk] = NR3(network1,slacknode,Vnom,[],[]);
        
        voltage(:,:,kt) = Vk;
        sendingPower(:,:,kt) = STXk;
        receivingPower(:,:,kt) = SRXk;
        
        % Compute the Objective
        Jk = compute_objective(objective, network1,Vk,Ik,STXk,SRXk,ik,sk);
        
        J(kt) = Jk;
        
        % store current state for plotting
        ii(:,:,kt) = ik;
        
        % obtain control
        if ESenabled
            for ph = 1:3
                for kn = 1:nnode

                    if fes(ph,kn)~=0

                        % real power ES loop
                        [uk, rhok, sigmak, xik, uhatk, ak] = ...
                            esfunction(time(kt), dt, hpfes(ph,kn), lpfes(ph,kn), fes(ph,kn), ...
                            kintes(ph,kn), au(ph,kn,kt-1), J(kt), J(kt-1), Jdot(kt-1), ...
                            rhou(ph,kn,kt-1), sigmau(ph,kn,kt-1), xiu(ph,kn,kt-1), uhat(ph,kn,kt-1), a0, 0);

                        % store internal states
                        rhou(ph,kn,kt) = rhok;
                        sigmau(ph,kn,kt) = sigmak;
                        xiu(ph,kn,kt) = xik;
                        au(ph,kn,kt) = ak;

                        % reactive power ES loop
                        [vk, rhok, sigmak, xik, vhatk, ak] = ...
                            esfunction(time(kt), dt, hpfes(ph,kn), lpfes(ph,kn), fes(ph,kn), ...
                            kintes(ph,kn), av(ph,kn,kt-1), J(kt), J(kt-1), Jdot(kt-1), ...
                            rhov(ph,kn,kt-1), sigmav(ph,kn,kt-1), xiv(ph,kn,kt-1), vhat(ph,kn,kt-1), a0, -pi/2);

                        % store interal states
                        rhov(ph,kn,kt) = rhok;
                        sigmav(ph,kn,kt) = sigmak;
                        xiv(ph,kn,kt) = xik;
                        av(ph,kn,kt) = ak;

                        % rectify average control (whatk) if control (wk) magnitude
                        % is greater than controller apparent power capacity
                        [uhatk, vhatk] = rectify_what_wmax(time(kt),uk,vk,uhatk,vhatk,fes(ph,kn),a0,wmaxpu(ph,kn));

                        % rectify average conrol (whatk) if control (wk) has
                        % positive real power component (consuming real power)
                        [uhatk, vhatk] = rectify_what_generation(time(kt),uk,vk,uhatk,vhatk,fes(ph,kn),a0);

                        % XXX
                        % Set maximum generation (negative value)
                        PVgenMax = PPV_matrix(:,:,kt);
                        [uhatk, vhatk] = rectify_what_umin(time(kt),uk,vk,uhatk,vhatk,fes(ph,kn),a0,-PVgenMax(ph,kn));

                        % recompute current real and reactive power control and
                        % store
                        uk = uhatk + a0*cos(2*pi*fes(ph,kn)*time(kt));
                        u(ph,kn,kt) = uk;
                        uhat(ph,kn,kt) = uhatk;

                        vk = vhatk + a0*sin(2*pi*fes(ph,kn)*time(kt));
                        v(ph,kn,kt) = vk;
                        vhat(ph,kn,kt) = vhatk;
                    end
                end
            end
        end
    end
end

toc

%%

close all
% % 
% % % figure, box on, hold on
% % % plot(time,J,'b-','LineWidth',2)
% % % set(gca,'FontSize',12,'FontWeight','bold','XTick',time(1):(time(end)-time(1))/12:time(end))%,'YTick',0:0.05:0.2)
% % % title('Objective Function','FontSize',12,'FontWeight','bold')
% % % xlabel('Time [s]','FontSize',12,'FontWeight','bold')
% % % % ylabel('J(u,v)','FontSize',12,'FontWeight','bold')
% % % % axis([time(1) time(end) 0 0.2])
% % % pbaspect([1 0.5 1])

% print('-f1','-depsc',['C:\Users\Michael\Desktop\temp\es\objective.eps'])

% figure, box on, hold on
% plot(time,MD(1,:),'r-','LineWidth',2)
% plot(time,MD(2,:),'g-','LineWidth',2)
% plot(time,MD(3,:),'b-','LineWidth',2)
% set(gca,'FontSize',12,'FontWeight','bold','XTick',0:60:600)
% title('Magnitude Difference','FontSize',12,'FontWeight','bold')
% xlabel('Time [s]','FontSize',12,'FontWeight','bold')
% ylabel('[p.u.^{2}]','FontSize',12,'FontWeight','bold')
% % axis([time(1) time(end) -5e-3 1.5e-2])
% pbaspect([1 0.5 1])
% print('-f2','-depsc',['C:\Users\Michael\Desktop\temp\es\magdif.eps'])
% 
% figure, box on, hold on
% plot(time,ED(1,:),'r-','LineWidth',2)
% plot(time,ED(2,:),'g-','LineWidth',2)
% plot(time,ED(3,:),'b-','LineWidth',2)
% set(gca,'FontSize',12,'FontWeight','bold','XTick',0:60:600)
% title('Magnitude Squared Difference','FontSize',12,'FontWeight','bold')
% xlabel('Time [s]','FontSize',12,'FontWeight','bold')
% ylabel('[p.u.^{2}]','FontSize',12,'FontWeight','bold')
% % axis([time(1) time(end) -5e-3 1.5e-2])
% pbaspect([1 0.5 1])
% print('-f3','-depsc',['C:\Users\Michael\Desktop\temp\es\mag2dif.eps'])
% 
% figure, box on, hold on
% plot(time,180/pi*AD(1,:),'r-','LineWidth',2)
% plot(time,180/pi*AD(2,:),'g-','LineWidth',2)
% plot(time,180/pi*AD(3,:),'b-','LineWidth',2)
% set(gca,'FontSize',12,'FontWeight','bold','XTick',0:60:600,'YTick',-0.10:0.10:0.40)
% title('Angle Difference','FontSize',12,'FontWeight','bold')
% xlabel('Time [s]','FontSize',12,'FontWeight','bold')
% ylabel('[deg]','FontSize',12,'FontWeight','bold')
% % axis([time(1) time(end) -0.10 0.40])
% pbaspect([1 0.5 1])
% print('-f4','-depsc',['C:\Users\Michael\Desktop\temp\es\angdif.eps'])
% 
% figure, box on, hold on
% plot(time,abs(PD(1,:)),'r-','LineWidth',2)
% plot(time,abs(PD(2,:)),'g-','LineWidth',2)
% plot(time,abs(PD(3,:)),'b-','LineWidth',2)
% set(gca,'FontSize',12,'FontWeight','bold','XTick',0:60:600,'YTick',0:0.0025:0.01)
% title('Phasor Difference','FontSize',12,'FontWeight','bold')
% xlabel('Time [s]','FontSize',12,'FontWeight','bold')
% ylabel('[p.u.]','FontSize',12,'FontWeight','bold')
% % axis([time(1) time(end) 0 0.01])
% pbaspect([1 0.5 1])
% print('-f5','-depsc',['C:\Users\Michael\Desktop\temp\es\phasordif.eps'])

for kn = 1:nnode
    if kn ~= slacknode
%         figure, box on, hold on
%         ibaltemp(:) = abs(sum(ii(:,kn,:),1));
%         plot(time,ibaltemp,'LineWidth',2)
%         set(gca,'FontSize',12,'FontWeight','bold','XTick',0:time(end)/4:time(end))
%         title(['Magnitude of Neutral Line Current at ' network1.nodes.nodelist{kn}],'FontSize',12,'FontWeight','bold')        
%         xlabel('Time [s]','FontSize',12,'FontWeight','bold')
%         ylabel('[p.u.]','FontSize',12,'FontWeight','bold')
%         pbaspect([1 0.5 1])        
    end
end

% figure, box on, hold on
% plot(time,u,'b-','LineWidth',2)
% plot(time,uhat,'g-','LineWidth',2)
% set(gca,'FontSize',12,'FontWeight','bold')
% title('Control','FontSize',12,'FontWeight','bold')
% xlabel('Time [s]','FontSize',12,'FontWeight','bold')
% % ylbabel('J(u,v)','FontSize',12,'FontWeight','bold')
% % plot(time,ustar,'k--','LineWidth',2)
% 
% figure, box on, hold on
% plot(time,v,'b-','LineWidth',2)
% plot(time,vhat,'g-','LineWidth',2)
% set(gca,'FontSize',12,'FontWeight','bold')
% title('Control','FontSize',12,'FontWeight','bold')
% xlabel('Time [s]','FontSize',12,'FontWeight','bold')
% % ylbabel('J(u,v)','FontSize',12,'FontWeight','bold')
% % plot(time,ustar,'k--','LineWidth',2)
% % if ESenabled
% % abcvec = {'a','b','c'};
% % for kn = 1:nnode
% %     for ph = 1:3    
% %         if network1.cons.wmaxpu(ph,kn) ~= 0
% %             figure, box on, hold on
% %             utemp(1,:) = u(ph,kn,:);
% %             plot(time,utemp,'b-','LineWidth',2)
% %             vtemp(1,:) = v(ph,kn,:);
% %             plot(time,vtemp,'g-','LineWidth',2)
% %             plot(time,sqrt(utemp.^2 + vtemp.^2),'r-','LineWidth',2)
% %             set(gca,'FontSize',12,'FontWeight','bold','XTick',time(1):(time(end)-time(1))/10:time(end))%,'YTick',-0.15:0.05:0.15)
% %             title(['Control on Phase ' abcvec{ph} ' at Node ' network1.nodes.nodelist{kn}],'FontSize',12,'FontWeight','bold')
% %             xlabel('Time [s]','FontSize',12,'FontWeight','bold')
% %             ylabel('[p.u.]','FontSize',12,'FontWeight','bold')
% %             legend({'u_{m}(t)','v_{m}(t)','|w_{m}(t)|'},'FontWeight','bold','location','southeast')
% %             axis([time(1) time(end) -network1.cons.wmaxpu(ph,kn) network1.cons.wmaxpu(ph,kn)])
% %             pbaspect([1 0.5 1])
% % %             print(['-f' num2str(5+kn)],'-depsc',['C:\Users\Michael\Desktop\temp\es\' 'w' network1.nodes.nodelist{connode(kn)} '.eps'])
% %         end
% %     end
% % end
% % end
end

function [network] = network_loader(network, PLoad_Matrix, QLoad_Matrix, kt)
    PLoad = PLoad_Matrix(:,:,kt);
    QLoad = QLoad_Matrix(:,:,kt);
    network.loads.ppu = PLoad;
    network.loads.qpu = QLoad;
    network.loads.spu = PLoad + 1i*QLoad;
end

function [J] = compute_objective(objective, network1,Vk,Ik,STXk,SRXk,ik,sk)
%default is losses
    if strcmp(objective, 'VReg')
        J = compute_objective_voltage_regulation(network1,Vk,Ik,STXk,SRXk,ik,sk);
    elseif strcmp(objective, 'VBal')
        J = compute_objective_voltage_magnitude_balance(network1,Vk,Ik,STXk,SRXk,ik,sk);
    elseif strcmp(objective, 'INeutBal')
        J = compute_objective_current_balance(network1,Vk,Ik,STXk,SRXk,ik,sk);
    else
        J = compute_objective_losses(network1,Vk,Ik,STXk,SRXk,ik,sk);
    end
end