function [Jk] = compute_objective_current_balance(network1,Vk,Ik,STXk,SRXk,ik,sk)

base = network1.base;
nodes = network1.nodes;
lines = network1.lines;
loads = network1.loads;
caps = network1.caps;
cons = network1.cons;

sk = loads.spu.*(loads.aPQ + loads.aZ.*abs(Vk).^2 + loads.aZ.*abs(Vk).^2) + cons.wpu - 1j*caps.cappu;
sk(nodes.PH == 0) = 0;
% ss(:,:,kt) = sk;

ik = conj(sk./Vk);
ik(nodes.PH == 0) = 0;        
% ii(:,:,kt) = ik;

ibalk = sum(ik,1);
Jk = sum(abs(ibalk).^2,2);

end