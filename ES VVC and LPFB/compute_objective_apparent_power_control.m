function [Jk] = compute_objective_apparent_power_control(network1,Vk,Ik,STXk,SRXk,ik,sk)

base = network1.base;
nodes = network1.nodes;
lines = network1.lines;
loads = network1.loads;
caps = network1.caps;
cons = network1.cons;

Jk = sum(sum(abs(cons.wpu).^2));

end