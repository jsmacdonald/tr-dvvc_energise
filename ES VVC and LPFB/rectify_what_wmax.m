function [uhatk,vhatk] = rectify_what_wmax(tk,uk,vk,uhatk,vhatk,fes,ak,wmaxpu)

if sqrt(uhatk^2 + vhatk^2) >= wmaxpu - ak
    
   gamma = (wmaxpu - ak)/sqrt(uhatk^2 + vhatk^2);
   uhatk = gamma*uhatk;
   vhatk = gamma*vhatk;
   
   whatk = uhatk + 1j*vhatk;
    
end

% if sqrt(uk^2 + vk^2) >= wmaxpu && wmaxpu > 0
%     wk = uk + 1j*vk;
%     %     whatk = uhatk + 1j*vhatk;
%     %     ck = wk - whatk;
%     %     ck = a0*(cos(2*pi*fes(ph,kn)*time(kt)) + 1j*sin(2*pi*fes(ph,kn)*time(kt)));
%     ck = ak*exp(1j*2*pi*fes*tk);
%     whatk = wmaxpu*wk/abs(wk) - ck;
%     uhatk = real(whatk);
%     vhatk = imag(whatk);
% end

% wk = uk + 1j*vk;
% whatk = uhatk + 1j*vhatk;
% ck = ak*exp(1j*2*pi*fes*tk);
% 
% if abs(whatk) <= wmaxpu && abs(wk) >= wmaxpu && wmaxpu > 0
%     
%     gamma = wmaxpu/abs(wk);
%     wbark = gamma*wk;
%     whatk = wbark - ck;
%     
% elseif abs(whatk) >= wmaxpu && abs(wk) >= wmaxpu && wmaxpu > 0
% 
%     delta = wmaxpu/abs(whatk);
%     wbark = delta*whatk;
%     whatk = wbark - ck;
%     
% end
% 
% uhatk = real(whatk);
% vhatk = imag(whatk);

end

