function [uhatk,vhatk] = rectify_what_umax(tk,uk,vk,uhatk,vhatk,fes,ak,umax)

% if uk <= umin && mod(2*pi*fes*tk,pi/2) ~= 0
%     hk = vhatk - uhatk*tan(2*pi*fes*tk);
% %     ck = a0*(cos(2*pi*fes*tk) + 1j*sin(2*pi*fes*tk));
%     ck = ak*exp(1j*2*pi*fes*tk);
%     whatk = 1j*hk - ck;
%     uhatk = real(whatk);
%     vhatk = imag(whatk);
% end

wk = uk + 1j*vk;
whatk = uhatk + 1j*vhatk;
ck = ak*exp(1j*2*pi*fes*tk);

if uhatk <= umax && uk >= umax && mod(2*pi*fes*tk,pi/2) ~= 0
    
    gamma = (umax - uhatk)/(ak*cos(2*pi*fes*tk));
    vmax = vhatk + gamma*ak*sin(2*pi*fes*tk);
    whatk = (umax + 1j*vmax) - ck;
    
    
elseif uhatk >= uhatk && uk >= umax && mod(2*pi*fes*tk,pi/2) ~= 0
    
    gamma = umax/uhatk;
    vmax = gamma*vhatk;
    whatk = (umax + 1j*vmax) - ck;
    
end

uhatk = real(whatk);
vhatk = imag(whatk);

end

